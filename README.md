# Proyecto Integrador Sintaxis y Semántica de Lenguajes

## Objetivo

Definir un lenguaje de programación mediante la especificación de su léxico, su sintaxis y su semántica. Construir los Analizadores Léxico y Sintáctico correspondientes.

### Opcional

Generación de código para una máquina virtual o creación de un intérprete para el lenguaje (o parte del mismo)

## Características

1. Nombre y descripción general del lenguaje.
2. Definición del léxico mediante expresiones regulares.
3. Definición formal de su sintaxis mediante una gramática en notación BNF.
4. Descripción de la semántica asociada a cada expresión del lenguaje, en lenguaje natural.
5. Gramática modificada (sin ambigüedad, factorizada y sin recursividad por izquierda) y TAS.
6. Analizador Léxico
7. Analizador Sintáctico
8. Generación de código para una máquina virtual o creación de un intérprete para el lenguaje (o parte del mismo)

## Reglas

1. El lenguaje permite valores de tipo Cadena o Número Entero.
2. Un programa es una secuencia de una o más funciones.
3. Una función posee cero o más parámetros de entrada y una sola salida.
4. Los parámetros se declaran sin asignación de tipo.
5. Se permiten expresiones aritméticas con los operadores +, -, /, *, todos definidos sobre números naturales y expresiones sobre cadenas incluyendo concatenación, prefijo de una longitud dada, sufijo de una longitud dada y reverso de una cadena.
6. Se permiten expresiones relacionales y lógicas.
7. Se permite una función condicional de la forma SI listaDePares SINO expresionAritmetica, donde listaDePares es una secuencia de 1 o más pares de la forma Condicion : ExpresionAritmeticaOCadena.
8. No se permite la estructura secuencial ni la asignación.
9. Se permite una función de entrada de datos, que devuelve el número o cadena ingresada por teclado.
10. La ejecución de un programa es un llamado a una función, con sus parámetros.
11. El resultado de la función principal se imprime en pantalla automáticamente cuando se ejecuta el programa.

## Puntaje

- Los ítems 1, 2 y 3 valen (en conjunto) 2,5 puntos
- Los ítems 4 y 5 valen (en conjunto) 2,5 puntos
- El ítem 6, vale 2,5 puntos
- El ítem 7, vale 2,5 puntos
- El punto 8 es opcional. Si se realiza, ayuda en la regularización o promoción, y mejora la nota final del alumno en la asignatura.
